# LazyDatePicker

#### 项目介绍

* 项目名称：LazyDatePicker
* 所属系列：openharmony的第三方组件适配移植
* 功能：日期选择器
* 项目移植状态：主功能完成
* 调用差异：无
* 开发版本：sdk6，DevEco Studio 2.2 Beta1
* 基线版本：Release v1.1.0

#### 效果演示

![输入图片说明](https://images.gitee.com/uploads/images/2021/0625/103828_8573e8b1_7648707.gif "LazyDatePicker.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中

```txt
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2.在entry模块下的build.gradle文件中

```text
dependencies {
    implementation('com.gitee.chinasoft_ohos:LazyDatePicker:1.0.0')
    ......
 }
```

3.lib模块下引用

```text
api 'net.yslibrary.keyboardvisibilityevent.ohos:keyboardvisibilityevent:1.0.0'
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

在xml布局中引用

```txt
<com.mikhaellopez.lazydatepicker.LazyDatePicker
        ohos:id="$+id:lazyDatePicker"
        ohos:height="match_content"
        ohos:width="match_content"
        app:ldp_text_color="#111"
        app:ldp_hint_color="#FFBCBCBC"
        app:ldp_show_full_date="true"
        app:ldp_date_format="1"/>
```

并在外部设置最大值及最小值

```txt
// Define min & max date for sample
        Date minDate = LazyDatePicker.stringToDate("01-01-2016", DATE_FORMAT);
        Date maxDate = LazyDatePicker.stringToDate("12-31-2018", DATE_FORMAT);
        
// Init LazyDatePicker
        LazyDatePicker lazyDatePicker = (LazyDatePicker) findComponentById(ResourceTable.Id_lazyDatePicker);
        lazyDatePicker.setMinDate(minDate);
        lazyDatePicker.setMaxDate(maxDate);
```

且支持设置监听

```txt
lazyDatePicker.setOnDatePickListener(new LazyDatePicker.OnDatePickListener() {
            @Override
            public void onDatePick(Date dateSelected) {
                new ToastDialog(MainAbilitySlice.this).setContentText( "Selected date: " + LazyDatePicker.dateToString(dateSelected, DATE_FORMAT)).show();
            }
        });

        lazyDatePicker.setOnDateSelectedListener(new LazyDatePicker.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Boolean dateSelected) {
                System.out.println("onDateSelected: " + dateSelected);
            }
        });
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

1.0.0

#### 版权和许可信息

LazyDatePicker by Lopez Mikhael is licensed under a [Apache License 2.0](./LICENSE)