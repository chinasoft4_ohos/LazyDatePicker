package com.mikhaellopez.lazydatepicker;

import net.yslibrary.ohos.sample.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.ohos.sample.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Copyright (C) 2020 Mikhael LOPEZ
 * Licensed under the Apache License Version 2.0
 *
 * @since 2021-06-28
 */
public class LazyDatePicker extends DependentLayout {
    protected static final int LENGTH_DATE_COMPLETE = 8;
    protected String date;
    protected DateFormat dateFormat;

    private TextField editLazyDatePickerReal;
    private DirectionalLayout layoutLazyDatePicker;
    private Text textLazyDatePickerDate;

    private Text textLazyDate1;
    private Component viewLazyDate1;
    private Text textLazyDate2;
    private Component viewLazyDate2;
    private Text textLazyDate3;
    private Component viewLazyDate3;
    private Text textLazyDate4;
    private Component viewLazyDate4;
    private Text textLazyDate5;
    private Component viewLazyDate5;
    private Text textLazyDate6;
    private Component viewLazyDate6;
    private Text textLazyDate7;
    private Component viewLazyDate7;
    private Text textLazyDate8;
    private Component viewLazyDate8;
    private Component contentComponent;

    // Properties
    private Color textColor;
    private Color hintColor;
    private Date minDate;
    private Date maxDate;
    private boolean keyboardVisible = false;
    private boolean shakeAnimationDoing = false;
    private boolean showFullDate = true;
    private OnDatePickListener onDatePickListener;
    private OnDateSelectedListener onDateSelectedListener;
    private Text.TextObserver textWatcher;
    private AnimatorProperty animatorProperty;
    private String cacheString = "";
    private ShapeElement transparentElement = new ShapeElement();
    private ShapeElement textColorElement = new ShapeElement();
    private ShapeElement hintColorElement = new ShapeElement();
    private KeyboardVisibilityEvent keyboardVisibilityEvent;

    /**
     * 默认构造方式
     *
     * @param context
     */
    public LazyDatePicker(Context context) {
        super(context);
    }

    /**
     * 布局引用构造方式
     *
     * @param context
     * @param attrSet
     */
    public LazyDatePicker(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet);
    }

    /**
     * 布局设置主题构造方式
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public LazyDatePicker(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }

    /**
     * 设置软键盘监听事件
     *
     * @param slice
     * @param rootComponent
     */
    public void setKeyboardVisibilityEvent(AbilitySlice slice, Component rootComponent) {
        keyboardVisibilityEvent = new KeyboardVisibilityEvent();
        keyboardVisibilityEvent.setAbilitySliceRoot(rootComponent);
        keyboardVisibilityEvent.registerEventListener(slice, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean b) {
                if (!b) {
                    editLazyDatePickerReal.clearFocus();
                }
            }
        });

        Window window = slice.getWindow();
        window.setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
    }

    private void init(Context context, AttrSet attrs) {
        contentComponent = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_layout_lazy_date_picker, null, false);
        addComponent(contentComponent);
        textColor = attrs.getAttr("ldp_text_color").get().getColorValue();
        hintColor = attrs.getAttr("ldp_hint_color").get().getColorValue();
        showFullDate = attrs.getAttr("ldp_show_full_date").get().getBoolValue();
        int dateFormatValue = attrs.getAttr("ldp_date_format").get().getIntegerValue();

        dateFormat = DateFormat.fromValue(dateFormatValue);
        transparentElement.setRgbColor(new RgbColor(0,0,0,0));
        textColorElement.setRgbColor(new RgbColor(0,0,0));
        hintColorElement.setRgbColor(new RgbColor(164, 164, 164));
        initView();
        onAttachedToWindow();
    }

    private void onAttachedToWindow() {
        if (showFullDate) {
            addKeyboardVisibilityListener(this, new OnKeyboardVisibilityListener() {
                @Override
                public void onVisibilityChange(boolean isVisible) {
                    if (keyboardVisible != isVisible) {
                        keyboardVisible = isVisible;
                        if (!keyboardVisible && editLazyDatePickerReal.isFocused()) {
                            editLazyDatePickerReal.clearFocus();
                        }
                    }
                }
            });
        }

        editLazyDatePickerReal.setFocusChangedListener(new FocusChangedListener() {
            @Override
            public void onFocusChange(Component component, boolean b) {
                showDate(date, b);
                if (showFullDate) {
                    showFullDateLayout(b);
                }
            }
        });

        initTextWatcher();
    }

    // region PRIVATE METHOD
    private void initView() {
        date = "";
        editLazyDatePickerReal = (TextField) contentComponent
                .findComponentById(ResourceTable.Id_edit_lazy_date_picker_real);
        layoutLazyDatePicker = (DirectionalLayout) contentComponent
                .findComponentById(ResourceTable.Id_layout_lazy_date_picker);
        layoutLazyDatePicker = (DirectionalLayout) contentComponent
                .findComponentById(ResourceTable.Id_layout_lazy_date_picker);
        textLazyDatePickerDate = (Text) contentComponent
                .findComponentById(ResourceTable.Id_text_lazy_date_picker_date);

        initialize();

        if (dateFormat == DateFormat.MM_DD_YYYY) {
            textLazyDate1.setText(getContext().getString(ResourceTable.String_ldp_month));
            textLazyDate2.setText(getContext().getString(ResourceTable.String_ldp_month));
            textLazyDate3.setText(getContext().getString(ResourceTable.String_ldp_day));
            textLazyDate4.setText(getContext().getString(ResourceTable.String_ldp_day));
        } else if (dateFormat == DateFormat.DD_MM_YYYY) {
            textLazyDate1.setText(getContext().getString(ResourceTable.String_ldp_day));
            textLazyDate2.setText(getContext().getString(ResourceTable.String_ldp_day));
            textLazyDate3.setText(getContext().getString(ResourceTable.String_ldp_month));
            textLazyDate4.setText(getContext().getString(ResourceTable.String_ldp_month));
        }

        textLazyDate5.setText(getContext().getString(ResourceTable.String_ldp_year));
        textLazyDate6.setText(getContext().getString(ResourceTable.String_ldp_year));
        textLazyDate7.setText(getContext().getString(ResourceTable.String_ldp_year));
        textLazyDate8.setText(getContext().getString(ResourceTable.String_ldp_year));

        textLazyDatePickerDate.setTextColor(textColor);

        contentComponent.findComponentById(ResourceTable.Id_btn_lazy_date_picker_on_focus)
                .setClickedListener(new ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        showKeyboard(editLazyDatePickerReal);
                    }
                });
    }

    private void initialize() {
        textLazyDate1 = (Text) contentComponent.findComponentById(ResourceTable.Id_text_lazy_date_1);
        viewLazyDate1 = contentComponent.findComponentById(ResourceTable.Id_view_lazy_date_1);
        textLazyDate2 = (Text) contentComponent.findComponentById(ResourceTable.Id_text_lazy_date_2);
        viewLazyDate2 = contentComponent.findComponentById(ResourceTable.Id_view_lazy_date_2);
        textLazyDate3 = (Text) contentComponent.findComponentById(ResourceTable.Id_text_lazy_date_3);
        viewLazyDate3 = contentComponent.findComponentById(ResourceTable.Id_view_lazy_date_3);
        textLazyDate4 = (Text) contentComponent.findComponentById(ResourceTable.Id_text_lazy_date_4);
        viewLazyDate4 = contentComponent.findComponentById(ResourceTable.Id_view_lazy_date_4);
        textLazyDate5 = (Text) contentComponent.findComponentById(ResourceTable.Id_text_lazy_date_5);
        viewLazyDate5 = contentComponent.findComponentById(ResourceTable.Id_view_lazy_date_5);
        textLazyDate6 = (Text) contentComponent.findComponentById(ResourceTable.Id_text_lazy_date_6);
        viewLazyDate6 = contentComponent.findComponentById(ResourceTable.Id_view_lazy_date_6);
        textLazyDate7 = (Text) contentComponent.findComponentById(ResourceTable.Id_text_lazy_date_7);
        viewLazyDate7 = contentComponent.findComponentById(ResourceTable.Id_view_lazy_date_7);
        textLazyDate8 = (Text) contentComponent.findComponentById(ResourceTable.Id_text_lazy_date_8);
        viewLazyDate8 = contentComponent.findComponentById(ResourceTable.Id_view_lazy_date_8);

        textLazyDate1.setTextColor(hintColor);
        viewLazyDate1.setBackground(hintColorElement);
        viewLazyDate1.setVisibility(Component.HIDE);
        textLazyDate2.setTextColor(hintColor);
        viewLazyDate2.setBackground(hintColorElement);
        viewLazyDate2.setVisibility(Component.HIDE);
        textLazyDate3.setTextColor(hintColor);
        viewLazyDate3.setBackground(hintColorElement);
        viewLazyDate3.setVisibility(Component.HIDE);
        textLazyDate4.setTextColor(hintColor);
        viewLazyDate4.setBackground(hintColorElement);
        viewLazyDate4.setVisibility(Component.HIDE);
        textLazyDate5.setTextColor(hintColor);
        viewLazyDate5.setBackground(hintColorElement);
        viewLazyDate5.setVisibility(Component.HIDE);
        textLazyDate6.setTextColor(hintColor);
        viewLazyDate6.setBackground(hintColorElement);
        viewLazyDate6.setVisibility(Component.HIDE);
        textLazyDate7.setTextColor(hintColor);
        viewLazyDate7.setBackground(hintColorElement);
        viewLazyDate7.setVisibility(Component.HIDE);
        textLazyDate8.setTextColor(hintColor);
        viewLazyDate8.setBackground(hintColorElement);
        viewLazyDate8.setVisibility(Component.HIDE);
    }

    private void showFullDateLayout(boolean hasFocus) {
        if (!hasFocus && date.length() == LENGTH_DATE_COMPLETE) {
            layoutLazyDatePicker.setVisibility(Component.INVISIBLE);
            textLazyDatePickerDate.setVisibility(Component.VISIBLE);
            textLazyDatePickerDate.setText(dateToString(getDate(),
                    dateFormat.getCompleteFormatValue()));
        } else if (layoutLazyDatePicker.getVisibility() == Component.INVISIBLE) {
            layoutLazyDatePicker.setVisibility(Component.VISIBLE);
            textLazyDatePickerDate.setVisibility(Component.HIDE);
        }
    }

    private boolean charIsValid(String mDate, char unicodeChar) {
        // Check if char is a digit
        if (!Character.isDigit(unicodeChar)) {
            return false;
        }

        int length = mDate.length();

        // Check Month & Day by dateFormat selected
        int value = Character.getNumericValue(unicodeChar);
        if (dateFormat == DateFormat.MM_DD_YYYY) {
            if (month(mDate, length, value, 1, 2, 3, 1)) return false;
        } else if (dateFormat == DateFormat.DD_MM_YYYY) {
            if (month(mDate, length, value, 3, 1, 1, 2)) return false;
        }

        // Check if date is between min & max date
        if (length > 3 && minDateIsNotNull()) {
            StringBuilder dateToCheckTmp = new StringBuilder(mDate + unicodeChar);
            while (dateToCheckTmp.length() < LENGTH_DATE_COMPLETE) {
                dateToCheckTmp.append("9");
            }
            if (checkMinDate(dateToCheckTmp)) {
                return false;
            }
        }

        if (length > 3 && maxDateIsNotNull()) {
            StringBuilder dateToCheckTmp = new StringBuilder(mDate + unicodeChar);
            while (dateToCheckTmp.length() < LENGTH_DATE_COMPLETE) {
                dateToCheckTmp.append("0");
            }
            if (checkMaxDate(dateToCheckTmp)) {
                return false;
            }
        }

        if (length > 6) {
            StringBuilder dateToCheckTmp = new StringBuilder(mDate + unicodeChar);
            while (dateToCheckTmp.length() < LENGTH_DATE_COMPLETE) {
                dateToCheckTmp.append("9");
            }
            return checkSameDate(dateToCheckTmp);
        }

        return true;
    }

    private boolean month(String mDate, int length, int value, int i, int i2, int i3, int i4) {
        if (length == 0 && value > i) { // M1
            return true;
        } else if (length == 1 && ((Character.getNumericValue(mDate.charAt(0)) == i && value > i2)
                || (Character.getNumericValue(mDate.charAt(0)) == 0 && value == 0))) { // M2
            return true;
        } else if (length == 2 && value > i3) { // D1
            return true;
        } else if (length == 3 && ((Character.getNumericValue(mDate.charAt(2)) == i3 && value > i4)
                || (Character.getNumericValue(mDate.charAt(2)) == 0 && value == 0))) { // D2
            return true;
        }
        return false;
    }

    private void showDate(String value, boolean hasFocus) {
        manageViewFocus(hasFocus, value.length());
        switch (value.length()) {
            case 0:
                textLazyDate1.setText(getContext().getString(dateFormat == DateFormat.MM_DD_YYYY
                        ? ResourceTable.String_ldp_month : ResourceTable.String_ldp_day));
                textLazyDate1.setTextColor(hintColor);
                break;
            case 1:
                setProperty(value, textLazyDate1, 0, textLazyDate2, dateFormat == DateFormat.MM_DD_YYYY
                        ? ResourceTable.String_ldp_month : ResourceTable.String_ldp_day);
                break;
            case 2:
                setProperty(value, textLazyDate2, 1, textLazyDate3, dateFormat == DateFormat.MM_DD_YYYY
                        ? ResourceTable.String_ldp_day : ResourceTable.String_ldp_month);
                break;
            case 3:
                setProperty(value, textLazyDate3, 2, textLazyDate4, dateFormat == DateFormat.MM_DD_YYYY
                        ? ResourceTable.String_ldp_day : ResourceTable.String_ldp_month);
                break;
            case 4:
                setProperty(value, textLazyDate4, 3, textLazyDate5, ResourceTable.String_ldp_year);
                break;
            case 5:
                setProperty(value, textLazyDate5, 4, textLazyDate6, ResourceTable.String_ldp_year);

                break;
            case 6:
                setProperty(value, textLazyDate6, 5, textLazyDate7, ResourceTable.String_ldp_year);
                break;
            case 7:
                setProperty(value, textLazyDate7, 6, textLazyDate8, ResourceTable.String_ldp_year);
                break;
            case 8:
                textLazyDate8.setTextColor(textColor);
                textLazyDate8.setText(getLetterAt(7, value));
                break;
            default:
                break;
        }
    }

    private void setProperty(String value, Text textLazyDate, int i, Text mtextLazyDate, int p) {
        textLazyDate.setTextColor(textColor);
        textLazyDate.setText(getLetterAt(i, value));
        mtextLazyDate.setText(getContext().getString(p));
        mtextLazyDate.setTextColor(hintColor);
    }

    private void manageViewFocus(boolean hasFocus, int valueLength) {
        if (hasFocus) {
            viewLazyDate1.setVisibility(Component.VISIBLE);
            viewLazyDate2.setVisibility(Component.VISIBLE);
            viewLazyDate3.setVisibility(Component.VISIBLE);
            viewLazyDate4.setVisibility(Component.VISIBLE);
            viewLazyDate5.setVisibility(Component.VISIBLE);
            viewLazyDate6.setVisibility(Component.VISIBLE);
            viewLazyDate7.setVisibility(Component.VISIBLE);
            viewLazyDate8.setVisibility(Component.VISIBLE);

            switch (valueLength) {
                case 0:
                    viewLazyDate1.setVisibility(Component.VISIBLE);
                    viewLazyDate1.setBackground(textColorElement);
                    viewLazyDate2.setBackground(hintColorElement);
                    break;
                case 1:
                    viewLazyDate1.setBackground(transparentElement);
                    viewLazyDate2.setVisibility(Component.VISIBLE);
                    viewLazyDate2.setBackground(textColorElement);
                    viewLazyDate3.setBackground(hintColorElement);
                    break;
                case 2:
                    viewLazyDate2.setBackground(transparentElement);
                    viewLazyDate3.setVisibility(Component.VISIBLE);
                    viewLazyDate3.setBackground(textColorElement);
                    viewLazyDate4.setBackground(hintColorElement);
                    break;
                case 3:
                    viewLazyDate3.setBackground(transparentElement);
                    viewLazyDate4.setVisibility(Component.VISIBLE);
                    viewLazyDate4.setBackground(textColorElement);
                    viewLazyDate5.setBackground(hintColorElement);
                    break;
                case 4:
                    viewLazyDate4.setBackground(transparentElement);
                    viewLazyDate5.setVisibility(Component.VISIBLE);
                    viewLazyDate5.setBackground(textColorElement);
                    viewLazyDate6.setBackground(hintColorElement);
                    break;
                case 5:
                    viewLazyDate5.setBackground(transparentElement);
                    viewLazyDate6.setVisibility(Component.VISIBLE);
                    viewLazyDate6.setBackground(textColorElement);
                    viewLazyDate7.setBackground(hintColorElement);
                    break;
                case 6:
                    viewLazyDate6.setBackground(transparentElement);
                    viewLazyDate7.setVisibility(Component.VISIBLE);
                    viewLazyDate7.setBackground(textColorElement);
                    viewLazyDate8.setBackground(hintColorElement);
                    break;
                case 7:
                    viewLazyDate7.setBackground(transparentElement);
                    viewLazyDate8.setVisibility(Component.VISIBLE);
                    viewLazyDate8.setBackground(textColorElement);
                    break;
                case 8:
                    viewLazyDate8.setBackground(transparentElement);
                    break;
                default:
                    break;
            }
        } else {
            viewLazyDate1.setVisibility(Component.HIDE);
            viewLazyDate2.setVisibility(Component.HIDE);
            viewLazyDate3.setVisibility(Component.HIDE);
            viewLazyDate4.setVisibility(Component.HIDE);
            viewLazyDate5.setVisibility(Component.HIDE);
            viewLazyDate6.setVisibility(Component.HIDE);
            viewLazyDate7.setVisibility(Component.HIDE);
            viewLazyDate8.setVisibility(Component.HIDE);
        }
    }

    /**
     * region PROTECTED METHOD
     */
    protected void onDatePick() {
        if (onDatePickListener != null) {
            onDatePickListener.onDatePick(getDate());
        }
    }

    /**
     * 日期选中回调
     */
    protected void onDateSelected() {
        if (onDateSelectedListener != null) {
            onDateSelectedListener.onDateSelected(getDate() != null);
        }
    }

    /**
     * 检测最小日期是否为空
     *
     * @return true/false
     */
    protected boolean minDateIsNotNull() {
        return minDate != null;
    }

    /**
     * 检测最大日期是否为空
     *
     * @return true/false
     */
    protected boolean maxDateIsNotNull() {
        return maxDate != null;
    }

    /**
     * 检测最小日期是否有效
     *
     * @param dateToCheckTmp
     * @return true/false
     */
    protected boolean checkMinDate(StringBuilder dateToCheckTmp) {
        Date realDateToCheckTmp = stringToDate(dateToCheckTmp.toString(), dateFormat.getValue());
        return realDateToCheckTmp == null || realDateToCheckTmp.before(minDate);
    }

    /**
     * 检测最大日期是否有效
     *
     * @param dateToCheckTmp
     * @return true/false
     */
    protected boolean checkMaxDate(StringBuilder dateToCheckTmp) {
        Date realDateToCheckTmp = stringToDate(dateToCheckTmp.toString(), dateFormat.getValue());
        return realDateToCheckTmp == null || realDateToCheckTmp.after(maxDate);
    }

    /**
     * 检测当前日期格式是否正确
     *
     * @param dateToCheckTmp 日期格式
     * @return true/false
     */
    protected boolean checkSameDate(StringBuilder dateToCheckTmp) {
        Date realDateToCheckTmp = stringToDate(dateToCheckTmp.toString(), dateFormat.getValue());
        return dateToString(realDateToCheckTmp, dateFormat.getValue()).equals(dateToCheckTmp.toString());
    }

    /**
     * 输入时动态改变颜色
     */
    protected void fillDate() {
        detachTextWatcher();
        editLazyDatePickerReal.setText(date);
        initTextWatcher();

        textLazyDate1.setTextColor(textColor);
        textLazyDate1.setText(getLetterAt(0, date));
        textLazyDate2.setTextColor(textColor);
        textLazyDate2.setText(getLetterAt(1, date));
        textLazyDate3.setTextColor(textColor);
        textLazyDate3.setText(getLetterAt(2, date));
        textLazyDate4.setTextColor(textColor);
        textLazyDate4.setText(getLetterAt(3, date));
        textLazyDate5.setTextColor(textColor);
        textLazyDate5.setText(getLetterAt(4, date));
        textLazyDate6.setTextColor(textColor);
        textLazyDate6.setText(getLetterAt(5, date));
        textLazyDate7.setTextColor(textColor);
        textLazyDate7.setText(getLetterAt(6, date));
        textLazyDate8.setTextColor(textColor);
        textLazyDate8.setText(getLetterAt(7, date));

        viewLazyDate1.setBackground(transparentElement);
        viewLazyDate2.setBackground(transparentElement);
        viewLazyDate3.setBackground(transparentElement);
        viewLazyDate4.setBackground(transparentElement);
        viewLazyDate5.setBackground(transparentElement);
        viewLazyDate6.setBackground(transparentElement);
        viewLazyDate7.setBackground(transparentElement);
        viewLazyDate8.setBackground(transparentElement);

        if (showFullDate) {
            showFullDateLayout(editLazyDatePickerReal.isFocused());
        }
    }

    /**
     * region PUBLIC METHOD
     *
     * @return 返回日期格式
     */
    public Date getDate() {
        if (date.length() == LENGTH_DATE_COMPLETE) {
            return stringToDate(date, dateFormat.getValue());
        }
        return null;
    }

    /**
     * 设置日期格式
     *
     * @param newDate
     * @return true/false
     */
    public boolean setDate(Date newDate) {
        String tmpDate = dateToString(newDate, dateFormat.getValue());

        if (tmpDate.length() != LENGTH_DATE_COMPLETE
                || (minDate != null && newDate.before(minDate))
                || (maxDate != null && newDate.after(maxDate))) {
            return false;
        }

        this.date = tmpDate;

        fillDate();

        return true;
    }

    /**
     * 设置最小日期
     *
     * @param minDate 最小日期格式
     */
    public void setMinDate(Date minDate) {
        this.minDate = (Date) minDate.clone();
        clear();
    }

    /**
     * 设置最大日期
     *
     * @param maxDate 最大日期格式
     */
    public void setMaxDate(Date maxDate) {
        this.maxDate = (Date) maxDate.clone();
        clear();
    }

    /**
     * 设置日期格式
     *
     * @param dateFormat 日期格式
     */
    public void setDateFormat(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
        clear();
    }

    /**
     * 清除
     */
    public void clear() {
        initView();
    }

    /**
     * 输入错误的动画效果
     */
    public void shake() {
        shakeView(layoutLazyDatePicker);
    }

    public void setOnDatePickListener(OnDatePickListener onDatePickListener) {
        this.onDatePickListener = onDatePickListener;
    }

    public void setOnDateSelectedListener(OnDateSelectedListener onDateSelectedListener) {
        this.onDateSelectedListener = onDateSelectedListener;
    }

    // region TEXT WATCHER
    private void detachTextWatcher() {
        editLazyDatePickerReal.removeTextObserver(textWatcher);
        textWatcher = null;
    }

    private void initTextWatcher() {
        if (textWatcher == null) {
            textWatcher = new Text.TextObserver() {
                @Override
                public void onTextUpdated(String s, int start, int before, int count) {
                    if (!shakeAnimationDoing) {
                        if (cacheString.length() > s.length()) {
                            // 删除
                            // Remove last char
                            if (date.length() > 0) {
                                date = date.substring(0, date.length() - 1);
                            }
                        } else if (date.length() < LENGTH_DATE_COMPLETE && s.length() > 0
                                && charIsValid(date, s.charAt(s.length() - 1))) {
                            // 增加
                            char unicodeChar = s.charAt(s.length() - 1);
                            date += unicodeChar;
                            if (date.length() == LENGTH_DATE_COMPLETE) {
                                onDatePick();
                            }
                        } else {
                            if (s.length() > 1) {
                                editLazyDatePickerReal.setText(s);
                            }
                            shakeView(layoutLazyDatePicker);
                        }
                        cacheString = s;
                        showDate(date, true);
                        onDateSelected();
                    }
                }
            };
            editLazyDatePickerReal.addTextObserver(textWatcher);
        }
    }

    private void addKeyboardVisibilityListener(final Component rootLayout,
                                               final OnKeyboardVisibilityListener onKeyboardVisibilityListener) {
        Rect r = new Rect();
        rootLayout.getWindowVisibleRect(r);
        int screenHeight = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getAttributes().height;

        // r.bottom is the position above soft keypad or device button.
        // if keypad is shown, the r.bottom is smaller than that before.
        int keypadHeight = screenHeight - r.bottom;

        boolean isVisible = keypadHeight > screenHeight * 0.15; // 0.15 ratio is perhaps
        // enough to determine keypad height.
        onKeyboardVisibilityListener.onVisibilityChange(isVisible);
    }

    /**
     * 添加关键按键监听
     *
     * @since 2021-06-28
     */
    private interface OnKeyboardVisibilityListener {
        void onVisibilityChange(boolean isVisible);
    }

    private void showKeyboard(TextField editText) {
        editText.requestFocus();
        showSoftInput();
    }

    /**
     * 显示软键盘
     *
     * @return true/false
     */
    public static boolean showSoftInput() {
        try {
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method startInput = inputClass.getMethod("startInput",int.class,boolean.class);
            return (boolean)startInput.invoke(object,1,true);
        } catch (InvocationTargetException e) {
            e.getMessage();
        } catch (NoSuchMethodException e) {
            e.getMessage();
        } catch (IllegalAccessException e) {
            e.getMessage();
        } catch (ClassNotFoundException e) {
            e.getMessage();
        }
        return false;
    }

    // region UTILS
    private String getLetterAt(int position, String value) {
        return String.valueOf(value.charAt(position));
    }

    private void shakeView(Component view) {
        shakeAnimationDoing = true;
        animatorProperty = view.createAnimatorProperty()
                .moveFromX(-15).moveToX(15)
                .setDuration(30)
                .setCurveType(Animator.CurveType.CYCLE)
                .setLoopedCount(3)
                .setDuration(150)
                .setStateChangedListener(new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {
                    }

                    @Override
                    public void onStop(Animator animator) {
                    }

                    @Override
                    public void onCancel(Animator animator) {
                    }

                    @Override
                    public void onEnd(Animator animator) {
                        shakeAnimationDoing = false;
                        animatorProperty.reset();
                    }

                    @Override
                    public void onPause(Animator animator) {
                    }

                    @Override
                    public void onResume(Animator animator) {
                    }
                });
        animatorProperty.start();
    }

    /**
     * 日期格式转字符串
     *
     * @param date 日期
     * @param pattern 字符串
     * @return 字符串
     */
    public static String dateToString(Date date, String pattern) {
        return new SimpleDateFormat(pattern, Locale.getDefault()).format(date);
    }

    /**
     * 字符串转日期格式
     *
     * @param date 需要转换的日期格式
     * @param format 需要转换的字符串
     * @return 转换完成的字符串
     */
    public static Date stringToDate(String date, String format) {
        try {
            return new SimpleDateFormat(format, Locale.getDefault()).parse(date);
        } catch (ParseException e) {
            e.getMessage();
        }
        return new Date();
    }

    /**
     * 输入完毕回调接口
     *
     * @since 2021-06-28
     */
    public interface OnDatePickListener {
        /**
         * 输入完毕回调接口
         *
         * @param dateSelected
         */
        void onDatePick(Date dateSelected);
    }

    /**
     * 输入完毕回调接口
     *
     * @since 2021-06-28
     */
    public interface OnDateSelectedListener {
        /**
         * 输入完毕回调接口
         *
         * @param dateSelected true/false
         */
        void onDateSelected(Boolean dateSelected);
    }

    /**
     * 日期枚举
     *
     * @since 2021-06-28
     */
    public enum DateFormat {
        /**
         * 日期格式
         */
        MM_DD_YYYY,
        /**
         * 日期格式
         */
        DD_MM_YYYY;

        /**
         * 获取日期格式
         *
         * @return String
         */
        public String getValue() {
            switch (this) {
                case MM_DD_YYYY:
                    return "MMddyyyy";
                case DD_MM_YYYY:
                    return "ddMMyyyy";
                default:
                    break;
            }
            throw new IllegalArgumentException("Not value available for this DateFormat: " + this);
        }

        /**
         * 获取字符串
         *
         * @return String
         */
        public String getCompleteFormatValue() {
            switch (this) {
                case MM_DD_YYYY:
                    return "MMM dd yyyy";
                case DD_MM_YYYY:
                    return "dd MMM yyyy";
                default:
                    break;
            }
            throw new IllegalArgumentException("Not value available for this DateFormat: " + this);
        }

        /**
         * 通过枚举获取日期格式
         *
         * @return 枚举类
         */
        public int getAttrValue() {
            switch (this) {
                case MM_DD_YYYY:
                    return 1;
                case DD_MM_YYYY:
                    return 2;
                default:
                    break;
            }
            throw new IllegalArgumentException("Not value available for this DateFormat: " + this);
        }

        /**
         * 返回日期格式
         *
         * @param value
         * @return DateFormat
         */
        public static DateFormat fromValue(int value) {
            switch (value) {
                case 1:
                    return MM_DD_YYYY;
                case 2:
                    return DD_MM_YYYY;
                default:
                    break;
            }
            throw new IllegalArgumentException("This value is not supported for DateFormat: " + value);
        }
    }

    /**
     * 清除焦点
     */
    public void clearFocus() {
        editLazyDatePickerReal.clearFocus();
    }
}
