package com.mikhaellopez.lazydatepicker;

import ohos.agp.components.AttrSet;
import ohos.app.Context;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * 本地日期选择类
 *
 * @since 2021-06-28
 */
public class LazyLocalDatePicker extends LazyDatePicker {
    private LocalDate minDate;
    private LocalDate maxDate;
    private OnLocalDatePickListener onLocalDatePickListener;
    private OnLocalDateSelectedListener onLocalDateSelectedListener;

    /**
     * 默认构造方式
     *
     * @param context
     */
    public LazyLocalDatePicker(Context context) {
        super(context);
    }

    /**
     * 布局引用构造方式
     *
     * @param context
     * @param attrSet
     */
    public LazyLocalDatePicker(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    /**
     * 布局设置主题构造方式
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public LazyLocalDatePicker(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    protected void onDatePick() {
        if (onLocalDatePickListener != null) {
            onLocalDatePickListener.onLocalDatePick(getLocalDate());
        }
    }

    @Override
    protected void onDateSelected() {
        if (onLocalDateSelectedListener != null) {
            onLocalDateSelectedListener.onLocalDateSelected(getLocalDate() != null);
        }
    }

    @Override
    protected boolean minDateIsNotNull() {
        return minDate != null;
    }

    @Override
    protected boolean maxDateIsNotNull() {
        return maxDate != null;
    }

    @Override
    protected boolean checkMinDate(StringBuilder dateToCheckTmp) {
        LocalDate realDateToCheckTmp = stringToLocalDate(dateToCheckTmp.toString(), dateFormat.getValue());
        return realDateToCheckTmp == null || realDateToCheckTmp.isBefore(minDate);
    }

    @Override
    protected boolean checkMaxDate(StringBuilder dateToCheckTmp) {
        LocalDate realDateToCheckTmp = stringToLocalDate(dateToCheckTmp.toString(), dateFormat.getValue());
        return realDateToCheckTmp == null || realDateToCheckTmp.isAfter(maxDate);
    }

    @Override
    protected boolean checkSameDate(StringBuilder dateToCheckTmp) {
        LocalDate realDateToCheckTmp = stringToLocalDate(dateToCheckTmp.toString(), dateFormat.getValue());
        return dateToString(realDateToCheckTmp, dateFormat.getValue()).equals(dateToCheckTmp.toString());
    }

    /**
     * region PUBLIC METHOD
     *
     * @return LocalDate
     */
    public LocalDate getLocalDate() {
        if (date.length() == LENGTH_DATE_COMPLETE) {
            return stringToLocalDate(date, dateFormat.getValue());
        }
        return null;
    }

    /**
     * 设置LocalDate
     *
     * @param newDate 需要设置的LocalDate
     * @return boolean
     */
    public boolean setLocalDate(LocalDate newDate) {
        String tmpDate = dateToString(newDate, dateFormat.getValue());

        if (tmpDate.length() != LENGTH_DATE_COMPLETE
                || (minDate != null && newDate.isBefore(minDate))
                || (maxDate != null && newDate.isAfter(maxDate))) {
            return false;
        }

        this.date = tmpDate;

        fillDate();

        return true;
    }

    /**
     * 设置最小的LocalDate
     *
     * @param minDate 需要设置的LocalDate
     */
    public void setMinLocalDate(LocalDate minDate) {
        this.minDate = minDate;
        clear();
    }

    /**
     * 设置最大的LocalDate
     *
     * @param maxDate 需要设置的LocalDate
     */
    public void setMaxLocalDate(LocalDate maxDate) {
        this.maxDate = maxDate;
        clear();
    }

    /**
     * 设置OnLocalDatePickListener
     *
     * @param onLocalDatePickListener
     */
    public void setOnLocalDatePickListener(OnLocalDatePickListener onLocalDatePickListener) {
        this.onLocalDatePickListener = onLocalDatePickListener;
    }

    /**
     * 设置OnLocalDateSelectedListener
     *
     * @param onLocalDateSelectedListener
     */
    public void setOnLocalDateSelectedListener(OnLocalDateSelectedListener onLocalDateSelectedListener) {
        this.onLocalDateSelectedListener = onLocalDateSelectedListener;
    }

    /**
     * 日期转字符串
     *
     * @param date
     * @param pattern
     * @return String
     */
    public static String dateToString(LocalDate date, String pattern) {
        return DateTimeFormatter.ofPattern(pattern).format(date);
    }

    /**
     * 时间转LocalDate
     *
     * @param date
     * @param format
     * @return LocalDate
     */
    public static LocalDate stringToLocalDate(String date, String format) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(format));
    }

    /**
     * 接口OnLocalDatePickListener
     *
     * @since 2021-06-28
     */
    public interface OnLocalDatePickListener {
        /**
         * 输入完毕自动回调
         *
         * @param dateSelected
         */
        void onLocalDatePick(LocalDate dateSelected);
    }

    /**
     * 接口OnLocalDateSelectedListener
     *
     * @since 2021-06-28
     */
    public interface OnLocalDateSelectedListener {
        /**
         * 输入完毕自动回调
         *
         * @param dateSelected
         */
        void onLocalDateSelected(Boolean dateSelected);
    }
}
