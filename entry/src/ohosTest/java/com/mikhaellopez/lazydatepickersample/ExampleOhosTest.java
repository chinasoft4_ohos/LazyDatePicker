package com.mikhaellopez.lazydatepickersample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    // entry下需要context，无法进行单元测试
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.mikhaellopez.lazydatepickersample", actualBundleName);
    }
}