/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mikhaellopez.lazydatepickersample.slice;

import com.mikhaellopez.lazydatepicker.LazyDatePicker;
import com.mikhaellopez.lazydatepickersample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;

import java.util.Date;

/**
 * MainAbilitySlice
 *
 * @since 2021-06-28
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final String DATE_FORMAT = "MM-dd-yyyy";
    private LazyDatePicker lazyDatePicker;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        // Define min & max date for sample
        Date minDate = LazyDatePicker.stringToDate("01-01-2016", DATE_FORMAT);
        Date maxDate = LazyDatePicker.stringToDate("12-31-2018", DATE_FORMAT);

        // Init View
        ((Text) findComponentById(ResourceTable.Id_textViewMinDate))
                .setText(LazyDatePicker.dateToString(minDate, DATE_FORMAT));
        ((Text) findComponentById(ResourceTable.Id_textViewMaxDate))
                .setText(LazyDatePicker.dateToString(maxDate, DATE_FORMAT));

        Component component = findComponentById(ResourceTable.Id_root);

        // Init LazyDatePicker
        lazyDatePicker = (LazyDatePicker) findComponentById(ResourceTable.Id_lazyDatePicker);
        lazyDatePicker.setKeyboardVisibilityEvent(this, component);

        lazyDatePicker.setMinDate(minDate);
        lazyDatePicker.setMaxDate(maxDate);

        lazyDatePicker.setOnDatePickListener(new LazyDatePicker.OnDatePickListener() {
            @Override
            public void onDatePick(Date dateSelected) {
                new ToastDialog(MainAbilitySlice.this)
                        .setContentText("Selected date: " + LazyDatePicker.dateToString(dateSelected, DATE_FORMAT))
                        .show();
            }
        });

        lazyDatePicker.setOnDateSelectedListener(new LazyDatePicker.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Boolean isDateSelected) {
            }
        });
    }
}